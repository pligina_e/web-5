window.alert("Добро Пожаловать!");
document.addEventListener("DOMContentLoaded", function () {
    window.console.log("DOM fully loaded and parsed");
});
function click1() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    if (!document.getElementById("coins").value.match(/^\d+$/)) {
        window.alert("В поле 'Цена товара' должно стоять числовое значение!");
    } else if (!document.getElementById("kolvo").value.match(/^\d+$/)) {
        window.alert("В поле 'Количество' должно стоять числовое значение!");
    } else {
        r.innerHTML = f1[0].value * f2[0].value;
    }
    return false;
}